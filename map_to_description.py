#! /usr/bin/env python3

"""Prints out the descriptions from a HexKit map file.

Handy if you use HexKit and print out paper maps.

Copyright (C) 2019  Nick Daly

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import sys
import textwrap


def main(amap):
    """Print out the listed map file's description."""

    map_data = get_map_data(amap)
    described_tiles = get_descriptions(map_data)
    print_map(map_data, described_tiles)


def get_map_data(mapfile):
    """Return the pythonic form of the map's data."""

    with open(mapfile) as infile:
        return json.loads(infile.read())


def get_descriptions(map_data):
    """Return a list of cells that have description data."""

    info = map_data['infoLayer']
    return [info.index(cell) for cell in info
            if cell['data'] or cell['label']['text']]


def print_map(map_data, described_tiles):
    """Print out the description information for each cell."""

    width = map_data['width']
    info = map_data['infoLayer']

    for tile in described_tiles:
        tile_info = "{:02d}{:02d}: {}".format(
            tile % width + 1,
            int(tile/width) + 1,
            info[tile]['label']['text'])

        if info[tile]['data']:
            tile_info += "\n{}".format(
                textwrap.indent(info[tile]['data'], 4 * " "))

        print(tile_info)


if __name__ == "__main__":
    for afile in sys.argv[1:]:
        print(afile + ":")
        main(afile)
        if afile != sys.argv[-1]:
            print("----")
